# Jobs Fixture Travel Exercise

Exercise Jobs Fixture Travel requested to complete. Project includes additional logic to the [Symfony Demo Application](https://github.com/symfony/demo) that sits inside a [Vagrant](https://www.vagrantup.com/) VM using the [Laravel Homestead](https://laravel.com/docs/5.7/homestead) Vagrant box. The project includes an additional page the user can select to view an image of myself along with a simlpe countdown for the Champions League Final for 2019. The page was built using PHP, Twig, HTML, SASS, and Javascript

# Requirements
Instructions are based on my Windows 10 machine
*  Oracle Virtual Box v6.0
*  Vagrant v2.2.3
*  Laravel Homestead v5.7
*  PHP v7.3.2

# Installation
*  [Enable Virtualization on Windows](https://www.tactig.com/enable-intel-vt-x-amd-virtualization-pc-vmware-virtualbox/)
*  Update hosts file located at: C:\Windows\System32\drivers\etc\hosts with the value below
>  192.168.10.10  homestead.test
*  Generate SSH Key with the name `id_rsa` and place it inside the `~/.ssh/` folder (mine was located at: C:\Users\jware\\.ssh). Command line execution:
>  ssh-keygen -t rsa -C "your_email@example.com"

# Usage
*  Pull repository
*  Open command prompt and `cd` into repository location
*  Start Vagrant box:
>  vagrant up
*  Open ssh into the VM (username and password should be `vagrant`):
>  vagrant ssh
*  Once logged in, `cd` to reach the source code located on the VM:
>  cd /vagrant/shared/symfony-demo
*  Start server:
>  php bin/console server:start 0.0.0.0:8000
*  On local host machine, open browser and go to site:
>  http://homestead.test:8000
*  Code I wrote will be located on `/self`
>  http://homestead.test:8000/self