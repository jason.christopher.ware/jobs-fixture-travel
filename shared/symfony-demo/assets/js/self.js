function treatAsUTC(date) {
  var result = new Date(date);
  result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
  return result;
}

function daysBetween(startDate, endDate) {
  var millisecondsPerDay = 24 * 60 * 60 * 1000;
  return Math.floor((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay);
}

$(function () {
  var endDate = new Date(2019, 6, 1);
  var days$ = document.getElementById('days');
  var hours$ = document.getElementById('hours');
  var minutes$ = document.getElementById('minutes');
  var seconds$ = document.getElementById('seconds');
  setInterval(function () {
    var date = new Date();
    const diff = new Date(endDate - date);
    days$.innerHTML = daysBetween(date, endDate);
    hours$.innerHTML = diff.getHours();
    minutes$.innerHTML = diff.getMinutes();
    seconds$.innerHTML = diff.getSeconds();
  }, 1000);
});