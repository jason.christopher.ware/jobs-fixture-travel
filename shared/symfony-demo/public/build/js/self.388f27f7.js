(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/self"],{

/***/ "./assets/js/self.js":
/*!***************************!*\
  !*** ./assets/js/self.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/* WEBPACK VAR INJECTION */(function($) {function treatAsUTC(date) {\n  var result = new Date(date);\n  result.setMinutes(result.getMinutes() - result.getTimezoneOffset());\n  return result;\n}\n\nfunction daysBetween(startDate, endDate) {\n  var millisecondsPerDay = 24 * 60 * 60 * 1000;\n  return Math.floor((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay);\n}\n\n$(function () {\n  var endDate = new Date(2019, 6, 1);\n  var days$ = document.getElementById('days');\n  var hours$ = document.getElementById('hours');\n  var minutes$ = document.getElementById('minutes');\n  var seconds$ = document.getElementById('seconds');\n  setInterval(function () {\n    var date = new Date();\n    var diff = new Date(endDate - date);\n    var absDiff = Math.abs(endDate - date); // const hours = Math.floor(absDiff/3.6e6);\n    // const minutes = Math.floor(absDiff/1000/60);\n    // const seconds = Math.floor(absDiff/1000/60/60);\n\n    days$.innerHTML = daysBetween(date, endDate);\n    hours$.innerHTML = diff.getHours();\n    minutes$.innerHTML = diff.getMinutes();\n    seconds$.innerHTML = diff.getSeconds();\n  }, 1000);\n});\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\")))\n\n//# sourceURL=webpack:///./assets/js/self.js?");

/***/ })

},[["./assets/js/self.js","runtime","vendors~js/admin~js/app~js/login~js/search~js/self"]]]);