(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/self"],{

/***/ "./assets/js/self.js":
/*!***************************!*\
  !*** ./assets/js/self.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/* WEBPACK VAR INJECTION */(function($) {function treatAsUTC(date) {\n  var result = new Date(date);\n  result.setMinutes(result.getMinutes() - result.getTimezoneOffset());\n  return result;\n}\n\nfunction daysBetween(startDate, endDate) {\n  var millisecondsPerDay = 24 * 60 * 60 * 1000;\n  return Math.floor((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay);\n}\n\n$(function () {\n  var date = new Date();\n  var endDate = new Date(2019, 6, 1);\n  setInterval(function () {\n    var diff = endDate - new Date();\n    var countdown$ = document.getElementById('countdown');\n    countdown$.innerHTML = new Date(diff);\n    console.log(\"\".concat(diff, \" - \").concat(countdown$, \" - \").concat(countdown$.innerHTML, \" - \").concat(daysBetween(new Date(), endDate)));\n  }, 1000);\n});\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\")))\n\n//# sourceURL=webpack:///./assets/js/self.js?");

/***/ })

},[["./assets/js/self.js","runtime","vendors~js/admin~js/app~js/login~js/search~js/self"]]]);