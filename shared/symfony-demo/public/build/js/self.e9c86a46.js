(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["js/self"],{

/***/ "./assets/js/self.js":
/*!***************************!*\
  !*** ./assets/js/self.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/* WEBPACK VAR INJECTION */(function($) {function _readOnlyError(name) { throw new Error(\"\\\"\" + name + \"\\\" is read-only\"); }\n\nfunction treatAsUTC(date) {\n  var result = new Date(date);\n  result.setMinutes(result.getMinutes() - result.getTimezoneOffset());\n  return result;\n}\n\nfunction daysBetween(startDate, endDate) {\n  var millisecondsPerDay = 24 * 60 * 60 * 1000;\n  return Math.floor((treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay);\n}\n\n$(function () {\n  var endDate = new Date(2019, 6, 1);\n  var countdown$ = document.getElementById('countdown');\n  var days$ = document.getElementById('days');\n  setInterval(function () {\n    var date = new Date();\n    var diff = new Date(endDate - date);\n    var absDiff = Math.abs(endDate - date);\n    var hours = absDiff / 3.6e6;\n    var minutes = absDiff = (_readOnlyError(\"absDiff\"), Math.floor(absDiff / 1000 / 60));\n    var seconds = absDiff = (_readOnlyError(\"absDiff\"), Math.floor(absDiff / 1000 / 60 / 60));\n    var days = daysBetween(date, endDate);\n    countdown$.innerHTML = \"\".concat(hours, \" hour(s) - \").concat(minutes, \" minute(s) - \").concat(seconds, \" second(s)\");\n    days$.innerHTML = days;\n    console.log(\"\".concat(hours, \" - \").concat(minutes, \" - \").concat(seconds));\n  }, 1000);\n});\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\")))\n\n//# sourceURL=webpack:///./assets/js/self.js?");

/***/ })

},[["./assets/js/self.js","runtime","vendors~js/admin~js/app~js/login~js/search~js/self"]]]);