<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller used to manage blog contents in the public part of the site.
 *
 * @Route("/self")
 *
 * @author Jason Ware <jason.christopher.ware@gmail.com>
 */
class SelfController extends AbstractController
{
  public function call(): Response
  {
    return $this->render('self/index.html.twig');
  }
}